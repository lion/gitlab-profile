# Lion

Lion is horizontally scalable [Minecraft: Java][minecraft] Edition server software. Its focus is efficiently scaling to
the needs of large mini-game networks.

Lion's components are well-documented. I.e., you can refer to the individual projects' README files for more information
about this server software's structure and inner workings.

[minecraft]: https://minecraft.net/
